const logger = require('../config/appconfig').logger
const database = require('../datalayer/mssql.dao')
const assert = require('assert')

module.exports = {
  createMovie: function(req, res, next) {
    logger.info('Post /api/movies aangeroepen')

    // Check dat we de UserId in het request hebben - vanuit de validateToken functie.
    logger.trace('User id:', req.userId)

    // hier komt in het request een movie binnen.
    const movie = req.body
    logger.info(movie)
    try {
      // Valideer hier de properties van de movie die we gaan maken.
      assert.equal(typeof movie.title, 'string', 'movie.title is required.')
    } catch (e) {
      const errorObject = {
        message: e.toString(),
        code: 400
      }
      return next(errorObject)
    }

    const query =
      "INSERT INTO Movie(Title, Description, Year, UserId) VALUES ('" +
      movie.title +
      "','" +
      movie.description +
      "','" +
      movie.year +
      "','" +
      req.userId +
      "'); SELECT * FROM Movie INNER JOIN DBUser ON Movie.UserId = DBUser.UserId WHERE MovieId = SCOPE_IDENTITY();"

    database.executeQuery(query, (err, rows) => {
      // verwerk error of result
      if (err) {
        const errorObject = {
          message: 'Er ging iets mis in de database.',
          code: 500
        }
        next(errorObject)
      }
      if (rows) {
        res.status(200).json({ result: rows.recordset })
      }
    })
  },

  getAllMovies: (req, res, next) => {
    logger.info('Get /api/movies aangeroepen')

    const query =
      'SELECT * FROM Movie ' +
      'INNER JOIN DBUser ON (Movie.UserId = DBUser.UserId) ' +
      ' JOIN Movie_Actor ON (Movie.MovieId = Movie_Actor.FilmId)'
    database.executeQuery(query, (err, rows) => {
      // verwerk error of result
      if (err) {
        const errorObject = {
          message: 'Er ging iets mis in de database.',
          code: 500
        }
        next(errorObject)
      }
      if (rows) {
        res.status(200).json({ result: rows.recordset })
      }
    })
  },

  getMovieById: function(req, res, next) {
    logger.info('Get /api/movies/id aangeroepen')
    const id = req.params.movieId

    const query = `SELECT * FROM Movie WHERE MovieId=${id};`
    database.executeQuery(query, (err, rows) => {
      // verwerk error of result
      if (err) {
        const errorObject = {
          message: 'Er ging iets mis in de database.',
          code: 500
        }
        next(errorObject)
      }
      if (rows) {
        res.status(200).json({ result: rows.recordset })
      }
    })
  },

  deleteMovieById: function(req, res, next) {
    logger.info('deleteById aangeroepen')
    const id = req.params.movieId
    const userId = req.userId

    const query = `DELETE FROM Movie WHERE MovieId=${id} AND UserId=${userId}`
    database.executeQuery(query, (err, rows) => {
      // verwerk error of result
      if (err) {
        logger.trace('Could not delete movie: ', err)
        const errorObject = {
          message: 'Er ging iets mis in de database.',
          code: 500
        }
        next(errorObject)
      }
      if (rows) {
        if (rows.rowsAffected[0] === 0) {
          // query ging goed, maar geen film met de gegeven MovieId EN userId.
          // -> retourneer een error!
          const msg = 'Movie not found or you have no access to this movie!'
          logger.trace(msg)
          const errorObject = {
            message: msg,
            code: 401
          }
          next(errorObject)
        } else {
          res.status(200).json({ result: rows })
        }
      }
    })
  },

  linkActorToMovie: function(req, res, next) {
    logger.info('linkActorToMovie aangeroepen')

    // Check dat we de UserId in het request hebben - vanuit de validateToken functie.
    logger.trace('User id:', req.userId)

    const movieId = req.params.movieId
    const actorId = req.params.actorId
    logger.info(`movieId = ${movieId}, actorId = ${actorId}`)

    const query = "INSERT INTO Movie_Actor (ActorId, FilmId) VALUES ('" + actorId + "','" + movieId + "')"

    database.executeQuery(query, (err, rows) => {
      // verwerk error of result
      if (err) {
        const errorObject = {
          message: 'Er ging iets mis in de database.',
          code: 500
        }
        next(errorObject)
      }
      if (rows) {
        res.status(200).json({ result: rows.recordset })
      }
    })
  }
}
